#ifndef BCM2835WRAPPER_H
#define BCM2835WRAPPER_H

#include <bcm2835.h>

class Bcm2835Wrapper
{
public:
    static Bcm2835Wrapper& getInstance();
    virtual ~Bcm2835Wrapper();

    // Rendre inaccessible les constructeurs de copie/déplacement
    // et opérateurs d'affectation/déplacement
    Bcm2835Wrapper(Bcm2835Wrapper const&) =delete;
    Bcm2835Wrapper(Bcm2835Wrapper&&) = delete;
    Bcm2835Wrapper& operator=(Bcm2835Wrapper const&) = delete;
    Bcm2835Wrapper operator=(Bcm2835Wrapper&&) = delete;

    typedef enum {LOGIC0=LOW, LOGIC1=HIGH} level_t;

    // Module GPIO
    typedef enum {INPUT, OUTPUT} gpio_mode_t;
    typedef enum {OFF, DOWN, UP} gpio_pud_t;
    void gpio_fsel(uint8_t pin, gpio_mode_t mode);
    void gpio_set_pud(uint8_t pin, gpio_pud_t pud);
    void gpio_clr(uint8_t pin);
    void gpio_set(uint8_t pin);
    uint8_t gpio_lev(uint8_t pin);

    // Module I2C
    typedef enum {STANDARD, FULL, FAST} i2c_clock_t;
    typedef enum {SUCCESS=BCM2835_I2C_REASON_OK
                  , NAK_ERR=BCM2835_I2C_REASON_ERROR_NACK
                  , CLK_STRETCH_ERR=BCM2835_I2C_REASON_ERROR_CLKT
                  , DATA_ERR=BCM2835_I2C_REASON_ERROR_DATA
                 } i2c_err_t;
    void i2c_setClock(i2c_clock_t clock);
    i2c_err_t i2c_write(uint8_t addr, uint8_t* buf, int len);
    i2c_err_t i2c_read(uint8_t addr, uint8_t* buf, int len);
    i2c_err_t i2c_write_read_rs(uint8_t addr, uint8_t* cmd, int cmdLen, uint8_t* buf, int bufLen);

    // Module SPI
    typedef enum {SPI_SCLK_100KHZ=100000, SPI_SCLK_1MHz=1000000} spi_clock_t;
    void spi_setClock(spi_clock_t clock);
    typedef enum : uint8_t {SPI_MODE_0=BCM2835_SPI_MODE0
        , SPI_MODE_1=BCM2835_SPI_MODE1
        , SPI_MODE_2=BCM2835_SPI_MODE2
        , SPI_MODE_3=BCM2835_SPI_MODE3
        } spi_mode_t;
    void spi_setDataMode(spi_mode_t mode);
    typedef enum  : uint8_t {SPI_CS0 = BCM2835_SPI_CS0
        , SPI_CS1 = BCM2835_SPI_CS1
        } spi_cs_t;
    void spi_chipSelect(spi_cs_t cs);
    void spi_setChipSelectPolarity(spi_cs_t cs, level_t activeLevel);
    void spi_writenb(uint8_t * tbuf, int len);
    void spi_transfernb(uint8_t * tbuf, uint8_t * rbuf, int len);

private:
    static const uint16_t _I2C_CLK_DIVIDER = BCM2835_I2C_CLOCK_DIVIDER_2500;
    static const uint16_t _SPI_CLK_DIVIDER = BCM2835_SPI_CLOCK_DIVIDER_64;
    Bcm2835Wrapper();
    uint16_t _getCoreClkMHz();
    uint16_t _coreFreqMHz;
};

#endif // BCM2835WRAPPER_H
