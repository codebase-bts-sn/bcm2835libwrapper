# Bcm2835LibWrapper

*Wrapper* C++ autour de la librairie [`bcm2835`](https://www.airspayce.com/mikem/bcm2835/index.html) de Mike McCauley.

![alt text](img/bcm2835wrapper.svg  "classe Bcm2835Wrapper")

> *NOTE :* Ne pas oublier de *linker* tout programme utilisant cette classe avec la librairie `bcm2835` (-> ex. : `g++ [...] -lbcm2835`)
